/*
Drew McDermott
adm75@pitt.edu
*/


//Networking
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//Threads
#include <pthread.h>

//C libs
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

//Macros
#define PORT_MIN 54500	
#define PORT_MAX 54599
#define CHUNK_S 1024
#define FILE_NAME "output.txt"

//Structs
typedef struct dl_args
{
	char* address;
	int port;

} dl_args;

//Prototype
void *download(void* args);
int writeData(int chunk, char* buffer, int size);

//MUTEX
pthread_mutex_t file_write = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t chunk_up = PTHREAD_MUTEX_INITIALIZER;

//Globals
int chunk_num = 0;

int main(int argc, char* argv[])
{
	pthread_t thread[100];
	dl_args args[100];
	int p_id;
	
	//Clear/make output file
	char mode[4] = "w";
	FILE* f = fopen(FILE_NAME, mode);
	fclose(f);

	
	int i = 0;
	int n_conns = (argc - 1)/2;
	
	printf("\n");
	
	for(i = 0;	i < n_conns; i++)
	{

		args[i].address = argv[i*2 + 1];		
		int port = atoi(argv[i*2 + 2]);
		if(port >= PORT_MIN && port <= PORT_MAX)
		{
			args[i].port = port;
		} else
		{
			printf("\nError: %d is out of range\n",port);
			continue;
		}
		
		printf("Starting download at %s on port %d\n", args[i].address, port);
		
		
		p_id = pthread_create(&(thread[i]), NULL, download, (void*)&(args[i]));
	
	}
	for(i = 0;	i < n_conns; i++)
	{	
		pthread_join(thread[i], NULL);
		//printf("\nExit (%d): %d\n", i, );
	}
	
	printf("Download complete, results are in %s\n\n",FILE_NAME);
	


}



void *download(void* args) //int chunk, char* address, int port)
{
	
	dl_args arg =  *(dl_args *)args;

	struct sockaddr_in serv_addr;
	int res = 0;
	int rec = 1;
	
	int c_num;

	while(rec > 0)
	{
	
		//Get / update chunk num
		pthread_mutex_lock(&chunk_up);
		c_num= chunk_num;
		chunk_num ++;
		pthread_mutex_unlock(&chunk_up);
		
		
		//Get socket FD
		int sockFD = socket(PF_INET, SOCK_STREAM, IPPROTO_IP);
		if(sockFD == -1)
		{
			printf("\nError: Could not create a socket");
			pthread_exit(NULL);  //Error
		}
		
		//Make struct
		memset(&serv_addr, 0 , sizeof(struct sockaddr_in));
		 
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(arg.port);
		serv_addr.sin_addr.s_addr=inet_addr(arg.address);
		
			//printf("\nConnecting to %s", arg.address);
		
			//Connect
			res = connect(sockFD, (struct sockaddr *)&serv_addr, sizeof(struct sockaddr_in));
			
			if(res == -1)
			{

				printf("\nError: Could not connect to %s on %d\n",arg.address,arg.port);
				pthread_exit(NULL);  //Error
			}
			
			//printf("\nConnected to %s", arg.address);
			

	//// Send & Recieve ////
			
			//Send chunk #
			char f_chunk[100];
			sprintf(f_chunk,"%d", c_num);
			
			//printf("\nChunk %s\n", f_chunk);
			
			res = send(sockFD, &f_chunk, strlen(f_chunk), 0);
			
			if(res < strlen(f_chunk))
			{
				printf("\nError: Data not sent");
				pthread_exit(NULL);
			}
			
			char buff[CHUNK_S];
			int size = CHUNK_S;
			
			rec = 0;
			while(rec < size)
			{
				
				res = recv(sockFD, &buff + rec, size - rec, 0);
				
				rec += res;
				
				if(res <= 0)
				{
					
					break;
				}
					
				
				
				
				
				//Yield here
				pthread_yield();
			}
			

			//Done recv
			if(rec > 0)
			{
				pthread_mutex_lock(&file_write);
				
				writeData(c_num,buff,rec);
				
				pthread_mutex_unlock(&file_write);
			}
			
			
			//Yield here
			
			pthread_yield();
			//printf("\nrec = %d, res = %d, buff = %X\n", rec, res, (buff + rec));
		
		
		//Close connection
		shutdown(sockFD, 2);
		close(sockFD);
	}
	
	

}

int writeData(int chunk, char* buffer, int size)
{
	char mode[4] = "a+b";
	
	char blank[CHUNK_S+1];
	char read[CHUNK_S];
	
	memset(blank,'X',CHUNK_S);
	blank[CHUNK_S] = 0;
	
	//printf("\n%s\n",blank);

	FILE* f = fopen(FILE_NAME, mode);
	
	if(f == NULL)
	{
		return -1; //Failure - FOpen error
	}
	
	
	
	fseek(f,0,SEEK_SET); //Go to beginning
	
	int r_size;
	int w_size;
	int i = 0;
	while(i < chunk-1)
	{
		
		r_size = fread(read, 1, CHUNK_S,f);
		//printf("\n r-size: %d \n",r_size);
		
		if(r_size < CHUNK_S)
		{
			//printf("\n1\n");
			w_size = fprintf(f, "%s", blank); //Print filler
			//printf("\n w-size: %d \n",w_size);
		}
		
		i++;
	}

	
	
	fwrite(buffer, 1, size, f);
	fclose(f);
	
	return 0;	

}

//EOF
